
(function () {
    'use strict';

    define(['MARFEEL_CONSTANTS', 'RestClient'], VisitsService);

    function VisitsService(MARFEEL_CONSTANTS, RestClient) {
        return {
            /**
             * Gets data regarding visits
             *
             * @returns Promise promise of resolved visits data
             */
            getVisits: getVisits
        };

        function getVisits() {
            return RestClient.get(MARFEEL_CONSTANTS.REST.VISITS_URL);
        }
    }
})();
