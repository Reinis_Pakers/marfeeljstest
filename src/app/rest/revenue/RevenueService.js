
(function () {
    'use strict';

    define(['MARFEEL_CONSTANTS', 'RestClient'], RevenueService);

    function RevenueService(MARFEEL_CONSTANTS, RestClient) {
        return {
            /**
             * Gets data regarding revenue
             *
             * @returns Promise promise of resolved revenue data
             */
            getRevenue: getRevenue
        };

        function getRevenue() {
            return RestClient.get(MARFEEL_CONSTANTS.REST.REVENUE_URL);
        }
    }
})();
