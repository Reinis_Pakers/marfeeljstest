(function () {
    'use strict';

    var express = require('express');
    var path = require('path');
    var app = express();

    app.use(express.static(__dirname + '/src'));

    app.get('/', function (req, res) {
        res.sendFile(path.join(__dirname + '/src/index.html'));
    });

    app.listen(3000);
    console.log('Server started at localhost:3000');
}());