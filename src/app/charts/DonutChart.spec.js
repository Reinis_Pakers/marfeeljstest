
(function () {
    'use strict';

    define(['d3', 'Utils', 'DonutChart'], function (d3, Utils, DonutChart) {
        describe('DonutChart', function () {
            beforeEach(function () {
                var self = this;

                spyOn(Utils, 'merge').and.callThrough();
                spyOn(Utils, 'formatNumber');

                this.donutChart = DonutChart.create({
                    showCurrency: true,
                    data: {
                        sectors: [
                            {
                                "device": "device",
                                "percentage": 60,
                                "amount": 1
                            },
                            {
                                "device": "device",
                                "percentage": 60,
                                "amount": 2
                            }
                        ],
                        trends: [
                            {
                                "x": 0,
                                "y": 519
                            }
                        ]
                    },
                    colors: ['#000000']
                });
            });

            it('merges properties', function () {
                expect(Utils.merge).toHaveBeenCalled();
            });

            it('formats sectors amount numbers', function () {
                expect(Utils.formatNumber).toHaveBeenCalledWith(1, true);
                expect(Utils.formatNumber).toHaveBeenCalledWith(2, true);
            });
        });
    });
})();
