
(function () {
    'use strict';

    define(['MARFEEL_CONSTANTS'], RestClient);

    function RestClient(MARFEEL_CONSTANTS) {
        return {
            /**
             * Loads external resource by provided URL
             *
             * @param {String} url
             */
            get: get
        };

        function get(url) {
            return new Promise(httpRequestExecutor);

            function httpRequestExecutor(resolve, reject) {
                var request = new XMLHttpRequest();
                request.overrideMimeType('application/json');
                request.open('GET', url, true);
                request.onreadystatechange = onreadystatechange;
                request.send(null);

                function onreadystatechange() {
                    if (request.readyState === MARFEEL_CONSTANTS.REST.HTTP_READY_STATE) {
                        if (request.status === MARFEEL_CONSTANTS.REST.OK_RESPONSE) {
                            resolve(JSON.parse(request.responseText));
                        } else {
                            reject(request);
                        }
                    }
                }
            }
        }
    }
})();
