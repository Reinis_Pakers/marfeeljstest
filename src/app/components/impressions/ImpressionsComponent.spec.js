
(function () {
    'use strict';

    define(['ImpressionsComponent', 'ImpressionsService', 'Utils', 'DonutChart'], function (ImpressionsComponent, ImpressionsService, Utils, DonutChart) {
        describe('ImpressionsComponent', function () {
            beforeEach(function () {
                var self = this;

                this.mockDataResponse = {
                    sectors: [
                        {
                            color: 1
                        },
                        {
                            color: 2
                        }
                    ]
                };

                spyOn(ImpressionsService, 'getImpressions').and.returnValue({
                    then: function (callback) {
                        callback(self.mockDataResponse);
                    }
                });
                spyOn(Utils, 'map');
                spyOn(DonutChart, 'create');

                ImpressionsComponent.init();
            });

            it('calls donut chart constructor', function () {
                expect(DonutChart.create).toHaveBeenCalledWith(jasmine.any(Object));
            });

            it('gets visits data', function () {
                expect(ImpressionsService.getImpressions).toHaveBeenCalled();
            });

            it('Maps colors', function () {
                expect(Utils.map).toHaveBeenCalledWith(this.mockDataResponse.sectors, 'color');
            });
        });
    });
})();
