
(function () {
    'use strict';

    define(['RevenueComponent', 'RevenueService', 'Utils', 'DonutChart'], function (RevenueComponent, RevenueService, Utils, DonutChart) {
        describe('RevenueComponent', function () {
            beforeEach(function () {
                var self = this;

                this.mockDataResponse = {
                    sectors: [
                        {
                            color: 1
                        },
                        {
                            color: 2
                        }
                    ]
                };

                spyOn(RevenueService, 'getRevenue').and.returnValue({
                    then: function (callback) {
                        callback(self.mockDataResponse);
                    }
                });
                spyOn(Utils, 'map');
                spyOn(DonutChart, 'create');

                RevenueComponent.init();
            });

            it('calls donut chart constructor', function () {
                expect(DonutChart.create).toHaveBeenCalledWith(jasmine.any(Object));
            });

            it('gets visits data', function () {
                expect(RevenueService.getRevenue).toHaveBeenCalled();
            });

            it('Maps colors', function () {
                expect(Utils.map).toHaveBeenCalledWith(this.mockDataResponse.sectors, 'color');
            });
        });
    });

})();
