
define(['VisitsService', 'RestClient'], function (VisitsService, RestClient) {
    describe('VisitsService', function () {
        it('gets impression data', function () {
            spyOn(RestClient, 'get');
            VisitsService.getVisits();

            expect(RestClient.get).toHaveBeenCalledWith(jasmine.any(String));
        });
    });
});
