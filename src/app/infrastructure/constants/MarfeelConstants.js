(function () {
    'use strict';

    define(MarfeelConstants);

    function MarfeelConstants() {
        return {
            POSITION: {
                RIGHT: 'right',
                LEFT: 'LEFT'
            },
            CURRENCY: '€',
            CURRENCY_SEPARATOR: '.',
            REST: {
                HTTP_READY_STATE: 4,
                OK_RESPONSE: 200,
                REVENUE_URL: 'app/rest/mocks/RevenueMockResponse.json',
                IMPRESSIONS_URL: 'app/rest/mocks/ImpressionsMockResponse.json',
                VISITS_URL: 'app/rest/mocks/VisitsMockResponse.json'
            }
        };
    }
})();