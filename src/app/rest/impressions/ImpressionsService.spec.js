
define(['ImpressionsService', 'RestClient'], function (ImpressionsService, RestClient) {
    describe('ImpressionsService', function () {
        it('gets impression data', function () {
            spyOn(RestClient, 'get');
            ImpressionsService.getImpressions();

            expect(RestClient.get).toHaveBeenCalledWith(jasmine.any(String));
        });
    });
});
