
(function () {
    'use strict';

    define(['VisitsService', 'DonutChart', 'Utils'], ImpressionsComponent);

    function ImpressionsComponent(VisitsService, DonutChart, Utils) {
        return {
            init: init
        };

        function init() {
            return VisitsService.getVisits().then(_addDataToPieChart);

            function _addDataToPieChart(data) {
                var pieChartProperties = {
                    data: data,
                    colors: Utils.map(data.sectors, 'color')
                };

                return DonutChart.create(pieChartProperties)
            }
        }
    }
})();
