
define(['Utils'], function (Utils) {
    describe('Utils', function () {
        describe('object merging', function () {
            beforeEach(function () {
                this.object = {
                    a: 1,
                    b: 2
                };
            });

            it('merges properties', function () {
                Utils.merge(this.object, {
                    b: 4
                });

                expect(this.object).toEqual({
                    a: 1,
                    b: 4
                });
            });

            it('throws exception if', function () {
                expect(function () {
                    Utils.merge(this.object, null)
                }).toThrow();
            });
        });

        describe('object type', function () {
            it('return false for Number', function () {
                expect(Utils.isObject(2)).toBeFalsy();
            });

            it('return false for undefined', function () {
                expect(Utils.isObject(undefined)).toBeFalsy();
            });

            it('return true for object', function () {
                expect(Utils.isObject({})).toBeTruthy();
            });
        });

        describe('number formatter', function () {
            it('does not add separator if number is less than 3 digits', function () {
                expect(Utils.formatNumber(1)).toBe('1');
            });

            it('adds separator if number is more than 3 digits', function () {
                expect(Utils.formatNumber(1000000)).toBe('1.000.000');
            });
        });

        describe('array mapping', function () {
            beforeEach(function () {
                this.array = [
                    {
                        a: 1,
                        b: 1
                    },
                    {
                        a: 1,
                        b: 1
                    }
                ]
            });

            it('takes property from array', function () {
                expect(Utils.map(this.array, 'b')).toEqual([1, 1]);
            });
        });
    });
});
