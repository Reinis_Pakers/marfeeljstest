module.exports = function (config) {
    config.set({
        plugins: [
            'karma-jasmine',
            'karma-requirejs',
            'karma-coverage',
            'karma-chrome-launcher'
        ],
        frameworks: ['jasmine', 'requirejs'],
        files: [
            'test/test-main.js',
            {pattern: 'src/**/*', included: false},
            {pattern: 'src/app/rest/mocks/**/*', included: false, served: false},
            'src/app/app.js'
        ],
        exclude: [],
        preprocessors: {
            'src/app/**/*!(*.spec).js': ['coverage']
        },
        coverageReporter: {
            type: 'html',
            dir: 'coverage/'
        },
        reporters: ['progress'],
        port: 9876,
        colors: true,
        logLevel: config.LOG_INFO,
        autoWatch: true,
        browsers: ['Chrome'],
        singleRun: true,
        concurrency: Infinity,
        proxies: {
            '/app/rest/mocks': '/base/app/rest/mocks'
        }
    })
};
