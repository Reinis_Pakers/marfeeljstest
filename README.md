# Marfeel JavaScript test

## Prerequisities

Require NodeJs to be installed to be able run npm scripts and serve development ExpressJS service

### Installing

Run npm start

```
npm start
```

This will install all necessary dependencies, run unit tests and serve local server in localhost:3000 URL

## Running the tests

**karma start karma.conf.js
**

Coverage reports is under "coverage" folder