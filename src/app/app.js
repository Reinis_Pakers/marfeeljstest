
(function () {
    'use strict';

    require.config({
        paths: {
            // Components
            'RevenueComponent': 'components/revenue/RevenueComponent',
            'ImpressionsComponent': 'components/impressions/ImpressionsComponent',
            'VisitsComponent': 'components/visits/VisitsComponent',
            'DonutChart': 'charts/DonutChart',

            // Services
            'RevenueService': 'rest/revenue/RevenueService',
            'ImpressionsService': 'rest/impressions/ImpressionsService',
            'VisitsService': 'rest/visits/VisitsService',
            'RestClient': 'rest/RestClient',

            // Vendor
            'd3': '../vendor/d3',

            // Other
            'MARFEEL_CONSTANTS': 'infrastructure/constants/MarfeelConstants',
            'Utils': 'infrastructure/Utils'
        }
    });

    require(['RevenueComponent', 'ImpressionsComponent', 'VisitsComponent'], App);

    function App(RevenueComponent, ImpressionsComponent, VisitsComponent) {
        // Display all 3 charts synchronously
        RevenueComponent.init().then(ImpressionsComponent.init).then(VisitsComponent.init);
    }
})();
