
(function () {
    'use strict';

    define(['ImpressionsService', 'DonutChart', 'Utils'], ImpressionsComponent);

    function ImpressionsComponent(ImpressionsService, DonutChart, Utils) {
        return {
            init: init
        };

        function init() {
            return ImpressionsService.getImpressions().then(_addDataToPieChart);

            function _addDataToPieChart(data) {
                var pieChartProperties = {
                    data: data,
                    colors: Utils.map(data.sectors, 'color')
                };

                return DonutChart.create(pieChartProperties)
            }
        }
    }
})();
