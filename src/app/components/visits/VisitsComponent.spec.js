
(function () {
    'use strict';

    define(['VisitsComponent', 'VisitsService', 'Utils', 'DonutChart'], function (VisitsComponent, VisitsService, Utils, DonutChart) {
        describe('VisitsComponent', function () {
            beforeEach(function () {
                var self = this;

                this.mockDataResponse = {
                    sectors: [
                        {
                            color: 1
                        },
                        {
                            color: 2
                        }
                    ]
                };

                spyOn(VisitsService, 'getVisits').and.returnValue({
                    then: function (callback) {
                        callback(self.mockDataResponse);
                    }
                });

                spyOn(Utils, 'map');
                spyOn(DonutChart, 'create');

                VisitsComponent.init();
            });

            it('calls donut chart constructor', function () {
                expect(DonutChart.create).toHaveBeenCalledWith(jasmine.any(Object));
            });

            it('gets visits data', function () {
                expect(VisitsService.getVisits).toHaveBeenCalled();
            });

            it('Maps colors', function () {
                expect(Utils.map).toHaveBeenCalledWith(this.mockDataResponse.sectors, 'color');
            });
        });
    });

})();
