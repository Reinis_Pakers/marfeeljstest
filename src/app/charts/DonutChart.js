
(function () {
    'use strict';

    define(['Utils', 'd3', 'MARFEEL_CONSTANTS'], DonutChart);

    function DonutChart(Utils, d3, MARFEEL_CONSTANTS) {
        return {
            create: create
        };

        function create(properties) {
            var pieChartProperties = {
                width: 350,
                height: 250,
                outerRadius: 100,
                innerRadius: 90,
                showCurrency: false,
                data: {}
            };
            Utils.merge(pieChartProperties, properties);

            var TABLET_INDEX = 1;
            var PHONE_INDEX = 0;
            var color = d3.scale.ordinal().range(properties.colors);
            var donutChartData = pieChartProperties.data.sectors;

            var mainSvgElement = d3.select('#chart')
                .append('svg')
                .attr('width', pieChartProperties.width)
                .attr('height', pieChartProperties.height)
                .append('g')
                .attr('transform', 'translate(' + pieChartProperties.width / 2 + ',' + pieChartProperties.height / 2 + ')');

            _drawInnerChartArea();
            _drawDonutPie();

            _addChartCenterText();
            _addLegends(PHONE_INDEX, MARFEEL_CONSTANTS.POSITION.RIGHT);
            _addLegends(TABLET_INDEX, MARFEEL_CONSTANTS.POSITION.LEFT);

            function _drawInnerChartArea() {
                var lineChartData = pieChartProperties.data.trends;
                var margin = {
                    top: 20,
                    right: 5,
                    bottom: 40,
                    left: -50
                };
                var width = 100 - margin.left - margin.right;
                var height = 100 - margin.top - margin.bottom;

                var x = d3.scale.linear()
                    .domain([0, d3.max(lineChartData, function (d) {
                        return d.x;
                    })])
                    .range([0, width]);

                var y = d3.scale.linear()
                    .domain([0, d3.max(lineChartData, function (d) {
                        return d.y;
                    })])
                    .range([height, 0]);

                var area = d3.svg.area()
                    .x(function (d) {
                        return x(d.x);
                    })
                    .y0(height * 2)
                    .y1(function (d) {
                        return y(d.y);
                    });

                var line = d3.svg.line()
                    .x(function (d) {
                        return x(d.x);
                    })
                    .y(function (d) {
                        return y(d.y);
                    });

                mainSvgElement.append("g")
                    .attr({
                        transform: "translate(" + -70 + "," + 20 + ")"
                    })
                    .append("path")
                    .datum(lineChartData)
                    .attr("class", "area")
                    .attr("style", "fill:" + donutChartData[TABLET_INDEX].color)
                    .attr("d", area);

                mainSvgElement.append("g")
                    .append("path")
                    .attr({
                        transform: "translate(" + -70 + "," + 20 + ")",
                        stroke: donutChartData[TABLET_INDEX].color
                    })
                    .datum(lineChartData)
                    .attr({
                        class: 'line',
                        d: line,
                        rx: 11
                    });
            }

            function _drawDonutPie() {
                var arc = d3.svg.arc().outerRadius(pieChartProperties.outerRadius).innerRadius(pieChartProperties.innerRadius);
                var pie = d3.layout.pie().value(_getPercentage).sort(null);

                mainSvgElement.append("circle")
                    .attr("r", 130);

                mainSvgElement.selectAll('.arc')
                    .data(pie(donutChartData))
                    .enter().append('g')
                    .attr('class', 'arc')
                    .append('path')
                    .attr('d', arc)
                    .style('fill', function (data) {
                        return color(data.data.device);
                    });
            }

            function _addLegends(index, position) {
                var rightPosition = position === MARFEEL_CONSTANTS.POSITION.RIGHT;
                var mainSvgElementRadius = pieChartProperties.width / 2;
                var x = rightPosition ? mainSvgElementRadius : -mainSvgElementRadius;
                var anchor = rightPosition ? 'end' : 'start';

                mainSvgElement.append('text')
                    .attr({
                        x: x,
                        y: 100,
                        'class': 'legend-header',
                        'text-anchor': anchor,
                        'fill': properties.colors[index]
                    })
                    .text(donutChartData[index].device)
                    .append('tspan')
                    .attr({
                        x: x,
                        y: 125,
                        'class': 'legend-percentage-text',
                        'text-anchor': anchor
                    })
                    .text(donutChartData[index].percentage + '%')
                    .append('tspan')
                    .attr({
                        dx: '15',
                        'class': 'legend-amount-text'
                    })
                    .text(Utils.formatNumber(donutChartData[index].amount, pieChartProperties.showCurrency));
            }

            function _addChartCenterText() {
                mainSvgElement.append('text')
                    .attr({
                        x: 0,
                        y: -30,
                        'text-anchor': 'middle',
                        'class': 'title'
                    })
                    .text(pieChartProperties.data.title);

                mainSvgElement.append('text')
                    .attr({
                        x: 0,
                        y: 0,
                        'text-anchor': 'middle',
                        'class': 'title-amount'
                    })
                    .text(Utils.formatNumber(pieChartProperties.data.number, pieChartProperties.showCurrency));
            }

            function _getPercentage(d) {
                return d.percentage;
            }
        }
    }
})();