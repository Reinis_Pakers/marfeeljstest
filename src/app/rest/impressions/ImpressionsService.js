
(function () {
    'use strict';

    define(['MARFEEL_CONSTANTS', 'RestClient'], ImpressionService);

    function ImpressionService(MARFEEL_CONSTANTS, RestClient) {
        return {
            /**
             * Gets data regarding impressions
             *
             * @returns Promise promise of resolved impressions data
             */
            getImpressions: getImpressions
        };

        function getImpressions() {
            return RestClient.get(MARFEEL_CONSTANTS.REST.IMPRESSIONS_URL);
        }
    }
})();