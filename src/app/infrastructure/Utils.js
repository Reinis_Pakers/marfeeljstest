
(function () {
    'use strict';

    define(['MARFEEL_CONSTANTS'], Utils);

    function Utils(MARFEEL_CONSTANTS) {
        return {
            /**
             * Merges object object properties
             *
             * @param {Object} object
             * @param {Object} source
             *
             * @returns {Object} merged object data
             */
            merge: merge,

            /**
             * Checks whether provided parameter is type of object
             *
             * @param {*} source
             *
             * @returns {Boolean} True - if parameter is object, false - otherwise
             */
            isObject: isObject,

            /**
             * Formats given number by adding thousands separator
             *
             * @param {Number} number
             * @param {Boolean} addCurrencySymbol Whether add currency symbol for the string
             *
             * @returns {String} Formatted string
             */
            formatNumber: formatNumber,

            /**
             * Maps property from given array
             *
             * @param {Array} array
             * @param [String} property
             *
             * @returns {Array} array of mapped properties
             */
            map: map
        };

        function merge(object, source) {
            if (!isObject(object)) {
                throw 'object must be type of object';
            }

            for (var propertyName in source) {
                if (source.hasOwnProperty(propertyName)) {
                    object[propertyName] = source[propertyName];
                }
            }
        }

        function isObject(source) {
            return typeof source === 'object';
        }

        function formatNumber(number, addCurrencySymbol) {
            var result = number;
            if (number) {
                result = String(result);

                var length = result.length;
                var separatedNumbers = [];
                for (var index = 3; index < length; index += 3) {
                    separatedNumbers.push(result.slice(-index).slice(0, 3));
                }

                var tempResult = separatedNumbers.join('.');

                if (tempResult) {
                    result = result.slice(0, Math.abs(separatedNumbers.length * 3 - result.length)) + '.' + tempResult;
                }

                result += addCurrencySymbol ? ' ' + MARFEEL_CONSTANTS.CURRENCY : '';
            }

            return result;
        }

        function map(array, property) {
            return array.map(function (element) {
                return element[property];
            })
        }
    }
})();
