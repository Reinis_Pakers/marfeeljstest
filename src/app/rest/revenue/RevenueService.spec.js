
define(['RevenueService', 'RestClient'], function (RevenueService, RestClient) {
    describe('RevenueService', function () {
        it('gets impression data', function () {
            spyOn(RestClient, 'get');
            RevenueService.getRevenue();

            expect(RestClient.get).toHaveBeenCalledWith(jasmine.any(String));
        });
    });
});
