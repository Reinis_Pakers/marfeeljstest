
(function () {
    'use strict';

    define(['RevenueService', 'DonutChart', 'Utils'], RevenueComponent);

    function RevenueComponent(RevenueService, DonutChart, Utils) {
        return {
            init: init
        };

        function init() {
            return RevenueService.getRevenue().then(_addDataToPieChart);

            function _addDataToPieChart(data) {
                var pieChartProperties = {
                    data: data,
                    showCurrency: true,
                    colors: Utils.map(data.sectors, 'color')
                };

                return DonutChart.create(pieChartProperties)
            }
        }
    }
})();
